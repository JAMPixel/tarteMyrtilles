#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VSOut {
	vec2 uv;
	vec2 tlUv;
	vec2 brUv;
	vec4 col;
} _in[];

out GSOut {
	vec2 uv;
	vec2 tlUv;
	vec2 brUv;
	vec4 col;
} _out;

void main() {
	for(int i = 0; i < 3; i++) {
		gl_Position = gl_in[i].gl_Position;
		_out.uv		= _in[i].uv;
		_out.tlUv	= _in[i].tlUv;
		_out.brUv	= _in[i].brUv;
		_out.col	= _in[i].col;
		EmitVertex();
	}
	EndPrimitive();
}
