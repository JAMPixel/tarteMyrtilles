#version 330

uniform sampler2D spritesheet;

in GSOut {
	vec2 uv;
	vec2 tlUv;
	vec2 brUv;
	vec4 col;
} _in;

out vec4 col;

void main() {
	vec2 uv = mix(_in.tlUv, _in.brUv, _in.uv);
	col = _in.col * texture(spritesheet, uv);
}
 