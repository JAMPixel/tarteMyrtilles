#version 330

uniform mat3 pvMatrix;

in vec2 pos;
in vec4 col;
in mat3 transform;

out VSOut {
	vec2 uv;
	vec2 tlUv;
	vec2 brUv;
	vec4 col;
} _out;

void main() {
	gl_Position = vec4(pvMatrix * transform * vec3(pos, 1.0f), 1.0);
	_out.uv = pos;
	_out.tlUv = vec2(0.0);
	_out.brUv = vec2(1.0);
	_out.col = col;
}
