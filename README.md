# Tarte aux Myrtilles

#### 08/10/2016 — Sujet : *Gravité*

Principe de jeu
--
Regarder les jolis astéroïdes. Le gameplay est dans un DLC ;)

Commandes (résultats non contractuels)
--
| Joueur 1      | Joueur 2     | Commande       |
|---------------|--------------|----------------|
| Gauche/Droite | Q/D (ou A/D) | Pivoter        |
| Haut          | Z (ou W)     | Sauter/Booster |
| G             | L            | Tirer          |

Dépendances
--
    glk (git@gitlab.com:D-z/glk.git), tag "tarteMyrtilles"
     + Boost 1.59
     + GLM 0.9.7.1
     + Un compilo qui bugue pas trop (Clang 3.6.0 est OK)
    SDL2
    SDL2_image
    OpenGL
    GLM 0.9.7.1
    GLEW
    GLU

