#pragma once

#include <glm/vec2.hpp>
#include <string>
#include "PlayerIdx.h"
#include "Asteroid.h"

struct GameField;
struct Input;

struct Player {
	bool ball=false;
	bool asteroide=true;
	int numberAsteroide=1;
	void update(GameField &parent);
    float AngleDegre;

	int point=0;

	PlayerIdx index;
	glm::vec2 pos;
	glm::vec2 vel;
	std::string img = "data/licorne.png";

	Player();
	Player(glm::vec2 pos, int i);
};

