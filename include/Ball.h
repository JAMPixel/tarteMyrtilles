#pragma once

#include <glm/vec2.hpp>
#include <string>

struct GameField;

struct Ball {
	bool playgot = true;
	float radius=0.25f;
    int whichPlayer=0;
    void update(GameField &parent);
	glm::vec2 pos;
	glm::vec2 vel;
	std::string img = "data/balle.png";
	
	
	
	Ball();
	Ball(glm::vec2 pos);
	~Ball();
	
};
