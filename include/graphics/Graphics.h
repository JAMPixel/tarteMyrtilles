#pragma once

#include <iostream>
#include <array>
#include <future>

#include <SDL2/SDL.h>

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glk/util.h>
#include <glk/path.h>
#include <glk/gluon.h>

#include <glk/gl/Texture.h>

#include <glk/gl/Sampler.h>
#include <glk/gl/glChk.h>

#include <glk/gl/prog/Program.h>
#include <glk/gl/prog/Uniform.h>
#include <glk/gl/prog/Attrib.h>
#include <glk/gl/prog/Shader.h>

#include <glk/gl/Vbo.h>
#include <glk/gl/CachedVbo.h>
#include <glk/gl/Vao.h>

#include <glk/gl/vertexAttribs.h>

#include "Player.h"
#include "Asteroid.h"
#include "BlackHole.h"
#include "GameField.h"
#include "Goal.h"
#include "Ball.h"

namespace gl = glk::gl;
namespace gluon = glk::gluon;



struct Vertex {
    glm::vec2 pos;
    glm::vec4 col;
};

struct PlainDq {
    template <class Prog>
    PlainDq(Prog const &prog, gl::Vbo<Vertex> const &vbo)
            : transforms{gl::VboUsage::staticDraw}
            , vao{glk_gl_makeBoundVao(
                          prog,
                          (vbo)(.pos, pos)(.col, col),
                          (transforms / 1)(.transform)
                  )} { }

    gl::CachedVbo<glm::mat3> transforms;
    gl::VaoName vao;
};

struct PlainProg : gl::Program {
    using Program::Program;

    glk_gl_progUnif(glm::mat3, pvMatrix);
    glk_gl_progUnif(gl::Sampler2d, spritesheet);

    glk_gl_progAttr(glm::vec2, pos);
    glk_gl_progAttr(glm::vec4, col);
    glk_gl_progAttr(glm::mat3, transform);
};


class Graphics{

public:
    glm::mat3 pvMat;
    std::array <glk::gl::TextureName, 2> texPlayer;    // un e textur e est une image dans la carte graphique
    std::array <glk::gl::TextureName, 5> texAste;       //ATTENTION nbAsteo existe mais pas utiliser ici
    glk::gl::TextureName texGameField;
    glk::gl::TextureName texBall;
    glk::gl::TextureName texBlackHole;
    std::array <glk::gl::TextureName, 2> texGoal;


    Graphics(int width, int height, GameField &gameField);
    static glm::mat3 makeMatrix(float x, float y, float rot, float width, float height);

    void display(const Player &player, PlainProg &plainProg, int numPlayer, glm::vec2 pos);
    void display(const Asteroid &aste, PlainProg &plainProg, int numAste, glm::vec2 pos);
    void display(const BlackHole &blacky, PlainProg &plainProg, glm::vec2 pos);
    void display(const Goal &goal, PlainProg &plainProg, int i, glm::vec2 pos);
    void display(const GameField &field, PlainProg &plainProg, float width, float heigh);
    void display(const Ball &ball, PlainProg &plainProg, glm::vec2 pos);

    template <typename T>
    void display(const T &objet);


};