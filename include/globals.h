#pragma once

constexpr float pxPerMeter = 64.0f;
constexpr float metersPerPx = 1.0f / pxPerMeter;
