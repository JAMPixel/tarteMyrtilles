#pragma once

#include <glm/vec2.hpp>
#include <string>

#include "PlayerIdx.h"

struct GameField;

struct Goal {
	void update(GameField &parent);

	PlayerIdx owner;
	glm::vec2 pos;
	int asteroideId;
	std::string img = "data/goal.png";
	Goal(glm::vec2 pos,int i,int j);
	~Goal();
};
