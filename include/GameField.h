#pragma once

#include <vector>
#include <array>
#include <memory>
#include <string>

#include <glm/vec2.hpp>

struct Asteroid;
struct Player;
struct Ball;
struct BlackHole;
struct Goal;

struct GameField {
	GameField();
	~GameField();
	
	void update();
	
	std::vector<std::unique_ptr<Asteroid>> asteroids;
	std::array<std::unique_ptr<Player>, 2> players;
	std::array<std::unique_ptr<Goal>, 2> goals;
	std::unique_ptr<Ball> ball;
	std::unique_ptr<BlackHole> blackHole;
	const int nbAsteo = 10;
	std::string img = "data/night-sky.png";
};
