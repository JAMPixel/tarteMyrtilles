#pragma once

#include <array>
#include <cstddef>
#include <memory>

#include "PlayerIdx.h"

struct Input {
	Input();
	
	void update();
	
	bool leftHeld(PlayerIdx player) const;
	bool rightHeld(PlayerIdx player) const;
	bool upHeld(PlayerIdx player) const;
	
	bool upPressed(PlayerIdx player) const;
	bool shootPressed(PlayerIdx player) const;

private:
	std::uint8_t const *kbd;

	using PlayerState = std::array<bool, 4>;

	std::array<std::array<PlayerState, 2>, 2> states{};
};

extern std::unique_ptr<Input> gInput;
