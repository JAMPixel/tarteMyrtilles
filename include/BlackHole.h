#pragma once

#include <glm/vec2.hpp>
#include <string>

struct GameField;

struct BlackHole {
	void update(GameField &parent);
	
	glm::vec2 pos;
	std::string img = "data/lapinou-coeur.png";

	BlackHole(glm::vec2 pos);
};
