#pragma once

#include <glm/mat2x2.hpp>

#include <string>


struct GameField;

struct Asteroid {
	std::string img = "data/asteo.png";

	float getposx();
	float getposy();

	void update(GameField &parent);
	Asteroid(struct GameField &parent);
	~Asteroid();
	
	float radius=0.5f;
	
	float angleDeg;
	float period;
	glm::mat2 radii;
	
	glm::vec2 pos;
};
