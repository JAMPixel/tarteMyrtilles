#include "GameField.h"
#include "Ball.h"
#include "Asteroid.h"
#include "Player.h"
#include <glm/gtx/norm.hpp>
#include "Input.h"
#include <cmath>
#include <algorithm>
#include <glk/util.h>
#include <glm/glm.hpp>

void Ball::update(GameField &parent) {
    int i;
    unsigned long numberPlayer=parent.players.size(); // on prend tous les joueur
    for (i=0;i<numberPlayer;i++){
        if ((glm::length2(pos - parent.players[i]->pos)) < ((0.2f))){
            playgot=true;
            whichPlayer=i;
            parent.players[i]->ball=true;
        }
    }


    if(playgot){
        pos[0]=parent.players[whichPlayer]->pos[0]+0.5*cos(parent.players[whichPlayer]->AngleDegre*M_PI/180.0f);
        pos[1]=parent.players[whichPlayer]->pos[1]+0.5*sin(parent.players[whichPlayer]->AngleDegre*M_PI/180.0f);
    }
    else {

        constexpr float asteWeight = 2.0f;
        constexpr float blackHoleWeight = 1.0f;

        glm::vec2 acc;

        for (auto const &ast : parent.asteroids) {
            glm::vec2 delta = ast->pos - pos;
            if (!delta.x && !delta.y)
                continue;

            acc += asteWeight * glm::normalize(delta) / glm::dot(delta, delta);
        }
        glm::vec2 delta = pos;
        acc += asteWeight * glm::normalize(delta) / glm::dot(delta, delta);

        acc += blackHoleWeight * -glm::normalize(pos) / glm::dot(pos, pos);

        vel += acc / 60.0f;

        auto hitAste = std::find_if(
                begin(parent.asteroids), end(parent.asteroids),
                [this](auto const &p) {
                    auto delta = p->pos - pos;
                    return glm::dot(delta, delta) < glk::sqr(p->radius + radius);
                }
        );

        if (hitAste != end(parent.asteroids)) {
            Asteroid const &a = **hitAste;
            glm::vec2 delta = pos - a.pos;
            glm::vec2 deltaT = {-delta.y, delta.x};

            bool incoming = glm::dot(vel, delta) < 0.0f;

            glm::vec2 proj = glm::dot(vel, deltaT) * deltaT / glm::dot(deltaT, deltaT);

            pos = a.pos + delta / glm::length(delta) * (a.radius + radius);

            if (incoming) { // On va vers l'astéroïde
                vel = 0.8f * (2.0f * proj - vel); // rebond
            } else {
                vel = proj;
            }
        }

        pos += vel / 60.0f;
    }
}


Ball::Ball() {
	pos[1]=((float)rand()/RAND_MAX*400);
	pos[0]=((float)rand()/RAND_MAX*400);
	
}

Ball::Ball(glm::vec2 pos):pos(pos), vel{1.0f} { }

Ball::~Ball() = default;
