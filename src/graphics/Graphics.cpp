#include "graphics/Graphics.h"

#include <glk/gl/glChk.h>

#include "globals.h"
#include "Ball.h"


// Transform matrix factory
glm::mat3 Graphics::makeMatrix(float x, float y, float rot, float width, float height) {
    glm::mat3 tm;

    float angle = glk::deg2rad(rot);
    float s = std::sin(angle);
    float c = std::cos(angle);

    tm[0][0] = c  * height;
    tm[1][1] = c  * height;
    tm[0][1] = -s * width;
    tm[1][0] = s  * width;

    // position de a texture autoure du point origine
    tm[2][0] = x - (tm[0][0] * 0.5f + tm[1][0] * 0.5f);
    tm[2][1] = y - (tm[0][1] * 0.5f + tm[1][1] * 0.5f);

    return tm;
}






Graphics::Graphics(int width, int height, GameField &gameField) {


    glm::vec2 camSize{width * metersPerPx, height * metersPerPx};

    // permet d'avoir 64 pixel = 1
    glm::mat3 worldToView{
            1.0f / camSize.x, 0.0f, 0.0f,
            0.0f, 1.0f / camSize.y, 0.0f,
            0.5f, 0.5f, 1.0f
    };

    glm::mat3 viewToScreen{
            2.0f, 0.0f, 0.0f,
            0.0f, -2.0f, 0.0f,
            -1.0f, 1.0f, 1.0f
    };

    pvMat = viewToScreen * worldToView;


    // load des textures

    texPlayer[0]    = gl::loadTextureFromFile(gameField.players[0]->img);
    texPlayer[1]    = gl::loadTextureFromFile(gameField.players[1]->img);
    texAste[0]      = gl::loadTextureFromFile(gameField.asteroids[0]->img);
    texAste[1]      = gl::loadTextureFromFile(gameField.asteroids[1]->img);
    texAste[2]      = gl::loadTextureFromFile(gameField.asteroids[2]->img);
    texAste[3]      = gl::loadTextureFromFile(gameField.asteroids[3]->img);
    texAste[4]      = gl::loadTextureFromFile(gameField.asteroids[4]->img);
    texGoal[0]      = gl::loadTextureFromFile(gameField.goals[0]->img);
    texGoal[1]      = gl::loadTextureFromFile(gameField.goals[1]->img);
    texBall         = gl::loadTextureFromFile(gameField.ball->img);
    texBlackHole    = gl::loadTextureFromFile(gameField.blackHole->img);
    texGameField    = gl::loadTextureFromFile(gameField.img);
}



void Graphics::display(const Player &player, PlainProg &plainProg, int numPlayer, glm::vec2 pos){

    // sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des perso : position + rotation
    displayQueue.transforms.push_back(makeMatrix(pos[0], pos[1], 0.0f, 1.0f, 1.0f));
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texPlayer[numPlayer], gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}




void Graphics::display(const Asteroid &aste, PlainProg &plainProg, int numAste, glm::vec2 pos){

    // sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des asteroides : position + rotation
    auto mat = makeMatrix(pos[0], pos[1], 0.0f, aste.radius * 2.5f, aste.radius * 2.5f);
    displayQueue.transforms.push_back(mat);
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texAste[0], gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}


void Graphics::display(const BlackHole &blacky, PlainProg &plainProg, glm::vec2 pos){
// sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des asteroides : position + rotation
    auto mat = makeMatrix(pos[0], pos[1], 0.0f, 1.0f, 1.0f);
    //mat[0][0] = 5.0f;
    //mat[1][1] = 7.0f;
    displayQueue.transforms.push_back(mat);
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texBlackHole, gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}


void Graphics::display(const Goal &goal, PlainProg &plainProg, int i, glm::vec2 pos){
// sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des asteroides : position + rotation
    auto mat = makeMatrix(pos[0], pos[1], 0.0f, 1.0f, 1.0f);
    //mat[0][0] = 5.0f;
    //mat[1][1] = 7.0f;
    displayQueue.transforms.push_back(mat);
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texGoal[i], gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}


void Graphics::display(const GameField &field, PlainProg &plainProg, float width, float height){
// sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des asteroides : position + rotation
    displayQueue.transforms.push_back(glm::mat3{
	    width, 0.0f, 0.0f,
        0.0f, height, 0.0f,
        -0.5f * width, -0.5f * height, 1.0f
    });
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texGameField, gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}



void Graphics::display(const Ball &ball, PlainProg &plainProg, glm::vec2 pos){
// sommets du perso
    gl::Vbo <Vertex> plainVbo{gl::VboUsage::staticDraw, {
            {{0.0f, 0.0f}, glm::vec4{1.0f}}, // coord / couleur
            {{1.0f, 0.0f}, glm::vec4{1.0f}},
            {{0.0f, 1.0f}, glm::vec4{1.0f}},
            {{1.0f, 1.0f}, glm::vec4{1.0f}}
    }};

    PlainDq displayQueue{plainProg, plainVbo};

    // mise en place des asteroides : position + rotation
    auto mat = makeMatrix(pos[0], pos[1], 0.0f, 2.0f * ball.radius, 2.0f * ball.radius);
    //mat[0][0] = 5.0f;
    //mat[1][1] = 7.0f;
    displayQueue.transforms.push_back(mat);
    displayQueue.transforms.upload();

    glk_with(bind(plainProg), bind(displayQueue.vao), bind(texBall, gl::TexUnit{0})) {
        plainProg.pvMatrix = pvMat;
        plainProg.spritesheet = gl::Sampler2d{0};
        glChk glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0u, plainVbo.size(), displayQueue.transforms.size());
    }
}


