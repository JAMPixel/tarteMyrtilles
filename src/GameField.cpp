#include "GameField.h"

#include "Asteroid.h"
#include "BlackHole.h"
#include "Player.h"
#include "Ball.h"
#include "Goal.h"
#include "BlackHole.h"

GameField::GameField() {
	// creation des asteroides
	for (int i = 0; i < nbAsteo; ++i) {
		asteroids.push_back(std::make_unique<Asteroid>(*this));
	}
	
	// creation des joueurs
	players[0] = (std::make_unique <Player>(glm::vec2(0.0f, 0.0f), 1));
	players[1] = (std::make_unique <Player>(glm::vec2(8.0f, 2.0f), 0));

	// creation du trou noir
	blackHole = (std::make_unique <BlackHole>(glm::vec2(10.0f, 10.0f)));

	// creation de la balle
	ball = (std::make_unique <Ball>(glm::vec2((float)(rand())/RAND_MAX * 8, (float)(rand())/RAND_MAX * 8)));

	// creation des buts
	goals[0] = (std::make_unique <Goal>(glm::vec2((float)(rand())/RAND_MAX * 8, (float)(rand())/RAND_MAX * 8),(int)((float)(rand())/RAND_MAX * 8),1));
	goals[1] = (std::make_unique <Goal>(glm::vec2((float)(rand())/RAND_MAX * 8, (float)(rand())/RAND_MAX * 8),(int)((float)(rand())/RAND_MAX * 8),0));

}


GameField::~GameField() = default;

void GameField::update() {
	for (auto const &p : asteroids)
		if (p)
			p->update(*this);
	for (auto const &p : players)
		if (p)
			p->update(*this);
	for (auto const &p : goals)
		if (p)
			p->update(*this);
	if (ball)
		ball->update(*this);
}
