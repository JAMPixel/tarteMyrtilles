#include "Asteroid.h"
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

Asteroid::Asteroid(struct GameField &parent) {
	angleDeg = ((float) rand()) / RAND_MAX * 360;
	period = 10.0f;//((float) rand() / RAND_MAX * 6000) + 3000;// periode varie de 3000à9000
	
	glm::vec2 size{
		(float) rand() / RAND_MAX * 5.f + 0.5f,
		(float) rand() / RAND_MAX * 5.0f + 0.5f
	};
	
	float rotation = ((float) rand()) / RAND_MAX * 2.0f * M_PI;
	glm::vec2 toto{std::cos(rotation), std::sin(rotation)}; // Vecteur unitaire orienté
	glm::vec2 orthoto{-toto.y, toto.x};
	
	radii = {
		toto.x * size.x, toto.y * size.x,
		orthoto.x * size.y, orthoto.y * size.y
	};

    /*radius= 5; // rayon de l'asteroide*/

	update(parent);
}

Asteroid::~Asteroid() = default;

void Asteroid::update(GameField &parent) {
	angleDeg += 1 / period;
	
	if (angleDeg > 360) {
		angleDeg = angleDeg - 360;
	}
	
	float angleRad = angleDeg * M_PI / 180.0f;
	
	glm::vec2 toto{std::cos(angleRad), std::sin(angleRad)}; // Vecteur unitaire orienté
	
	pos = radii * toto;
}


float Asteroid::getposx(){
	return pos[0];
}

float Asteroid::getposy(){
	return pos[1];
}