#include "Input.h"

#include <SDL2/SDL_keyboard.h>

#include <glk/util.h>

std::unique_ptr<Input> gInput;

namespace keys {
	enum Keys {
		left, right, up, shoot
	};
}

Input::Input()
: kbd{SDL_GetKeyboardState(nullptr)} { }

void Input::update() {
	swap(states[0], states[1]);
	auto &currentState = states[0];
	
	currentState[0][keys::left] = kbd[SDL_SCANCODE_LEFT];
	currentState[0][keys::right] = kbd[SDL_SCANCODE_RIGHT];
	currentState[0][keys::up] = kbd[SDL_SCANCODE_UP];
	currentState[0][keys::shoot] = kbd[SDL_SCANCODE_L];
	
	currentState[1][keys::left] = kbd[SDL_SCANCODE_A];
	currentState[1][keys::right] = kbd[SDL_SCANCODE_D];
	currentState[1][keys::up] = kbd[SDL_SCANCODE_W];
	currentState[1][keys::shoot] = kbd[SDL_SCANCODE_G];
}

bool Input::leftHeld(PlayerIdx player) const {
	return states[0][glk::val(player)][keys::left];
}

bool Input::rightHeld(PlayerIdx player) const {
	return states[0][glk::val(player)][keys::right];
}

bool Input::upHeld(PlayerIdx player) const {
	return states[0][glk::val(player)][keys::up];
}

bool Input::upPressed(PlayerIdx player) const {
	return states[0][glk::val(player)][keys::up]
	   && !states[1][glk::val(player)][keys::up];
}

bool Input::shootPressed(PlayerIdx player) const {
	return states[0][glk::val(player)][keys::shoot]
	   && !states[1][glk::val(player)][keys::shoot];
}
