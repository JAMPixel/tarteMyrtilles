#include <iostream>

#include <cstdlib>
#include <ctime>

#include <glk/gl/glChk.h>

#include <SDL2/SDL.h>

#include "graphics/Graphics.h"
#include "GameField.h"
#include "globals.h"
#include "Ball.h"
#include "Input.h"

int main(int, char **) {
	constexpr int const windowWidth = 1024, windowHeight = 768;
	
	std::srand(std::time(nullptr));
	
	// Initialize the OpenGL context --------------------------------------
	gluon::Sdl sdl{SDL_INIT_VIDEO | SDL_INIT_EVENTS};
	
	constexpr int screen = 0;
	
	auto sdlWindow = gluon::createWindow(
		"glk GL test program",
		SDL_WINDOWPOS_CENTERED_DISPLAY(screen), SDL_WINDOWPOS_CENTERED_DISPLAY(screen),
		windowWidth, windowHeight,
		SDL_WINDOW_OPENGL
	);
	
	auto glContext = gluon::createGlContext(sdlWindow.get(), 3, 3, SDL_GL_CONTEXT_PROFILE_CORE);
	
	SDL_ShowWindow(sdlWindow.get());
	
	gInput = std::make_unique<Input>();
	
	gluon::initGlew();
	
	std::cout << "Initialized OpenGL " << gluon::glContextVersion() << std::endl;
	
	glChk glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glChk glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glChk glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glChk glEnable(GL_BLEND);
	glChk glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	auto nnfSampler = gl::makeNnfSampler();
	gl::bindSamplerEverywhere(nnfSampler);
	// ----------------------------------------------------------------
	 
	PlainProg plainProg{		// affichage du player

		gl::VertShader{"data/sprite.vert"_f},
		gl::GeomShader{"data/sprite.geom"_f},
		gl::FragShader{"data/sprite.frag"_f}
	};



	// Creation du moteur physique
	GameField gameField;

	// creation du moteur graphique
	Graphics graphics(windowWidth, windowHeight, gameField);


	SDL_Event ev;
	bool loop = true;
	do {
		while(SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT
                || (ev.type == SDL_KEYDOWN && ev.key.keysym.scancode == SDL_SCANCODE_ESCAPE))
                loop = false;
        }

		gInput->update();
		
		// DEPLACEMENT --------------------------------------
		gameField.update();
		
		// nettoyage de l'ecran
		glChk glClear(GL_COLOR_BUFFER_BIT);

		// AFFICHAGE -----------------------------------------

		// affichage du terrain
		graphics.display(gameField, plainProg, windowWidth * metersPerPx, windowHeight * metersPerPx);

		// affichage des asteroides
		for(int i = 0 ; i < gameField.nbAsteo ; ++i)
			graphics.display(*gameField.asteroids[i], plainProg, i, gameField.asteroids[i]->pos);

		// affichage des players
		for(int i = 0 ; i < 2 ; ++i)
			graphics.display(*gameField.players[i], plainProg, i, gameField.players[i]->pos);

		// affichage des goals
		for(int i = 0 ; i < 2 ; ++i)
			graphics.display(*gameField.goals[i], plainProg, i, gameField.goals[i]->pos);

		// affichage du blackHole
		graphics.display(*gameField.blackHole, plainProg, gameField.blackHole->pos);

		// affichage de la balle
		graphics.display(*gameField.ball, plainProg, gameField.ball->pos);


		SDL_GL_SwapWindow(sdlWindow.get());
		SDL_Delay(16u);
	} while(loop);
}
