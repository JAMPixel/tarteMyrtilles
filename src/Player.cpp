#include "Player.h"
#include "Input.h"
#include "GameField.h"
#include "Ball.h"
#include <glm/gtx/norm.hpp>

#include <cmath>

constexpr float playerRotSpeed = 5.0f;

void Player::update(GameField &parent) {
	int i = 0;
	float pos_x = 0; // le vecteur vitesse temporaire
	float pos_y = 0; // le vecteur vitesse temporaire
	float temp1 = 0, temp2 = 0;
	
	if (asteroide) {
		if (gInput->leftHeld(index)) { // soit on se deplace à gauche ou à droite
			AngleDegre += playerRotSpeed;
			if (AngleDegre > 360)
				AngleDegre -= 360;
		} else if (gInput->rightHeld(index)) {
			AngleDegre -= playerRotSpeed;
			if (AngleDegre < 0)
				AngleDegre += 360;
		}
		
		if (gInput->upPressed(index)) { // on saute/déplace
			asteroide = false;
			
			vel[0] = 0.5 * std::cos(AngleDegre * M_PI / 180.0f);
			vel[1] = 0.5 * std::sin(AngleDegre * M_PI / 180.0f);
			
		} else { //on se laisse trainer par l'asteroide
			pos[0] = parent.asteroids[numberAsteroide]->pos[0] +
			         parent.asteroids[numberAsteroide]->radius * std::cos(AngleDegre * M_PI / 180.0f);
			pos[1] = parent.asteroids[numberAsteroide]->pos[1] +
			         parent.asteroids[numberAsteroide]->radius * std::sin(AngleDegre * M_PI / 180.0f);
		}
		
	} else /* pas sur un astéroide */ {
		if (gInput->leftHeld(index)) { // soit on se deplace à gauche ou à droite
			AngleDegre += playerRotSpeed;
			if (AngleDegre > 360)
				AngleDegre -= 360;
		} else {
			if (gInput->rightHeld(index)) {
				AngleDegre -= playerRotSpeed;
				if (AngleDegre < 0)
					AngleDegre += 360;
			}
		}
		if (gInput->upHeld(index)) {
			pos[0] -= 0.01 * std::sin(AngleDegre * M_PI / 180.0f) + vel[0];
			pos[1] += 0.01 * std::cos(AngleDegre * M_PI / 180.0f) + vel[1];
		}
		
		//TODO: collision avec un astéroïde -> accroche, asteroide = true
		// Done
		unsigned long numberAstero = parent.asteroids.size(); // on prend tous les asteroide
		for (i = 0; i < numberAstero; i++) {
			if ((glm::length2(pos - parent.asteroids[i]->pos)) < ((0.1f))) {
				asteroide = true;
				numberAsteroide = i;
			}
		}
		
	}
	
	
	if ((ball) && (gInput->shootPressed(index))) {
		parent.ball->pos[0] = 0.f;
		parent.ball->pos[1] = 0.f;
		
		//parent.ball->vel[0] = 180;//*cos(AngleDegre*M_PI/180.0f); //1 = puissance de lancé maxi
		//parent.ball->vel[1] = 180;//*sin(AngleDegre*M_PI/180.0f);
		ball = false;
		parent.ball->playgot = false;
		
		
	}
	
	
	if (vel[0] > 0)
		vel[0] -= 0.005; // perte de la vitesse initiale
	if (vel[1] > 0)
		vel[1] -= 0.005;
}


Player::Player()
	: pos(0.0f, 0.0f) {
	
}

Player::Player(glm::vec2 pos, int i)
	: pos(pos) {
	if (!i)
		index = PlayerIdx::one;
	else
		index = PlayerIdx::two;
}

/*
glm::vec2 Player::lancer() {
    glm::vec2 power;
    power[0]=10;
    power[1]=10;

    return power;
}
 */